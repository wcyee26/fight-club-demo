# Fight Club Demo

Fight Club Demo is a simple beat-em up sandbox that being built for experimental purpose.

## License Free, Open Source

Sharing is caring, please feel free to use and share this project as much as you like. You can mod it, publish it, turn it upside down, I don't care. Just make the fun out of it.

## Software Requirement

- [Unreal Engine 4](https://www.unrealengine.com/en-US/blog) (4.17 or above)
- [Visual C++ Redistributable 2015](https://www.microsoft.com/en-us/download/details.aspx?id=48145)
- [Visual C++ Redistributable 2013](https://www.microsoft.com/en-us/download/details.aspx?id=40784)
- [Visual C++ Redistributable 2012](https://www.microsoft.com/en-us/download/details.aspx?id=30679)
- [Visual C++ Redistributable 2010 x64](https://www.microsoft.com/en-US/Download/confirmation.aspx?id=14632)
- [Visual C++ Redistributable 2010 x86](https://www.microsoft.com/en-us/download/details.aspx?id=5555)
- [Visual C++ Redistributable 2008](https://www.microsoft.com/en-us/download/details.aspx?id=11895)
- [Visual C++ Redistributable 2005 x64](https://www.microsoft.com/en-US/Download/confirmation.aspx?id=21254)
- [Visual C++ Redistributable 2005 x86](https://www.microsoft.com/en-us/download/details.aspx?id=3387)
- [Visual Studio 2015 or higher](https://www.visualstudio.com/downloads/)
- [Git LFS](https://git-lfs.github.com/)

### For Windows 8.1 User ONLY

The following packages must be installed in ascending order.

- [March 2014](https://support.microsoft.com/en-us/help/2919442/march-2014-servicing-stack-update-for-windows-8-1-and-windows-server-2)
- [April 2014](https://support.microsoft.com/en-us/help/2919355/windows-rt-8-1--windows-8-1--and-windows-server-2012-r2-update-april-2)
- [Universal C Runtime](https://support.microsoft.com/en-us/help/2999226/update-for-universal-c-runtime-in-windows)

**IMPORTANT:** Unreal Engine must have these packages in order to run properly.

## Getting Started

To get the project up and running, at root of project, open **DemoFightClub.uproject**.

### Compiling Animation Blueprint

At root of project, go to **Content** > **Current** > **Character** > **Blueprint** > **MainChar** and open **MainCharAnim.uasset**.

Run **Compile**.

At root of project, go to **Content** > **Current** > **Character** > **Blueprint** > **Brute_UE4** and open **Brute_UE4_Anim.uasset**.

Run **Compile**.

### Build and Play

In the **Editor** > **Toolbar**, run **Build**. 

After finishing building the project, run **Play**.

You are ready to rock and roll \m/.

## Control Settings

- **Forward** - W
- **Backward** - S
- **Move Left** - A
- **Move Right** - D

- **Light Attack** - Mouse Left-Click
- **Strong Attack** - Hold Mouse Left-Click
- **Sprint** - Hold Spacebar
- **Block** - Hold Left Shift
- **Lock Target** - Mouse Right-Click
- **Reset Camera** - Mouse Middle-Click

If you wish to change the control settings, you can change it in the **Editor** > **Settings** > **Project Settings** > **Engine** > **Input** > **Bindings**.

## Maps

By default, **FightLocation1.umap** map will be loaded in the editor and game. 

If you wish to change the map, you can change it in the **Editor** > **Settings** > **Project Settings** > **Project** > **Maps & Modes** > **Default Maps**.

Note: If you are having trouble loading the default map, try changing the map or create a new map for your own usage.

## Going Deeper

If you wish to know more about the blueprint logics of the player, a.i., level design and more, please follow me down to the river.

### Blueprint Logics

#### Main Character (Player)

To locate the blueprint of the main character, at root of project, go to **Content** > **Current** > **Character** > **Blueprint** > **MainChar** and open **MainChar.uasset**.

#### Brute (A.I.)

To locate the blueprint of the brute character, at root of project, go to **Content** > **Current** > **Character** > **Blueprint** > **Brute_UE4** and open **Brute_UE4.uasset**.

#### Level Design

To locate the blueprint of the level design, open it in the **Editor** > **Blueprints** > **Open Level Blueprint**.

### Understanding The Logics

Please check out the **documentation.pdf** at the root of project. 

This document will show you the details of the fighting mechanics, such as punching, target lock on, a.i. and etc. 

You should be able to have a clear idea how the fighting mechanics is being wired up in the blueprint after reading this document.

## THANK YOU
